# TbUtf8

## What is it?

TbUtf8 is on library for Lazarus (Free Pascal).

With this library, you can all easily use UTF8 special and combining characters like 'üäößẶặǺǻǼǽǞǟǍǎḂḃÞþÇçĆćĊċ...'.

## Features

- A String class
- You don't have to free it
- All indexes are character based
- Gives you a number of characters
- Gives a number of bytes (Length)
- Delete characters
- Insert characters
- Append characters
- Read / Write a character
- Load / Save to file
- Load / Save to streaming


## Download
### With Git
Open a terminal and go in your Lazarus workspace where you would store it.


```bash

git clone https://gitlab.com/FpTuxe/tbutf8.git
```

### Alternative over Web- Interface
On the top / right side of the web page, click the download button.
Unzip the package, and move the folder 'tbutf8' in your workspace.

## Use
Start Lazarus and open your project.
Lazarus->File->Open
  your workspace/tbutf8/src/tb_utf8.pas

Lazarus->Project->Add Editor File to Project

### Alternative
Start Lazarus and open your project.
Lazarus->Package->Open Package File (.lpk)
  your workspace/tbutf8/src/tbutf8.lpk

Now, click Use->Add to Project

## License
See COPYING.TXT

## Example
```
proceudre Demo01: Boolean;
var
  u: IbUtf8;
  i: Integer;
begin
  u:= TIbUtf8.Create('Thömäß');
  for i:= 1 to u.NumberOfChars do begin
    case u.Chars[i] of
      'ö': u.Chars[i]:= 'o';
      'ä': u.Chars[i]:= 'a';
      'ß': u.Chars[i]:= 's';
    end;
  end;
  if u.Text = 'Thomas' then begin
    WriteLn('That''s right!');
  end;
end;
```
