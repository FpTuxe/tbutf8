unit tb_utf8;

{
  File: tb_utf8.pas
  CreateDate: 02.10.2021
  ModifyDate: 18.11.2022
  Version: 1.1.0.3
  Author: Thomas Bulla

  See the file COPYING.TXT, included in this distribution,
  for details about the copyright.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
}

{$mode objfpc}{$H+}

interface

uses
  Classes
  ,SysUtils
  ,tb_utf8_combinings
{$IFDEF TB_OBJECTSTAT_ENABLED}
  ,tb_objects
{$ENDIF}
  ;

type
  EbUtf8 = class(Exception);

  TbUtf8Option = (
    ut8Combining,
    ut8HkCombiningAsOneChar,           //'K͟H K͟h'
    ut8InclusiveCurrentChar );

  TbUtf8Options = set of TbUtf8Option;

 { IbUtf8 }

 IbUtf8 = interface
   function GetChar(Index: Cardinal): String;
   procedure SetChar(Index: Cardinal; aValue: String);
   function GetCurrentIndex: Cardinal;
   function GetOptions: TbUtf8Options;
   function GetText: String;
   function FirstChar: String;
   function LastChar: String;
   function NextChar: String;
   function BackChar: String;
   function IndexOf(const aValue: String): Cardinal; overload;
   function IndexOf(const aValue: String;
       StartIndex: Cardinal): Cardinal; overload;
   function LastIndexOf(const aValue: String): Cardinal; overload;
   function LastIndexOf(const aValue: String;
       StartIndex: Cardinal): Cardinal; overload;
   procedure SetCurrentIndex(AValue: Cardinal);
   procedure SetOptions(AValue: TbUtf8Options);
   procedure SetText(AValue: String);
   procedure LoadFromFile(const FileName: String);
   procedure LoadFromStream(Stream: TStream);
   procedure SaveToFile(const FileName: String);
   procedure SaveToStream(Stream: TStream);
   function CurrentChar: String;
   function NumberOfChars: Cardinal;
   function IsEmpty: Boolean;
   function IsEqual(const aText: String): Boolean;
   function IsStringEnd: Boolean;
   function IsStringFront: Boolean;
   function Length: Cardinal;
   procedure Append(aValue: IbUtf8);
   procedure Append(const aValue: String);
   function Copy: IbUtf8;
   function Copy(StartIndex, nChars: Cardinal): IbUtf8;
   function CopyAsString(StartIndex, nChars: Cardinal): String;
   procedure Delete(CharIndex: Cardinal); overload;
   procedure Delete(CharIndex, nChars: Cardinal); overload;
   procedure Insert(StartIndex: Cardinal; const aValue: String); overload;
   procedure Insert(StartIndex: Cardinal; aValue: IbUtf8); overload;
   property Chars[Index: Cardinal]: String read GetChar write SetChar;
   property CurrentIndex: Cardinal read GetCurrentIndex write SetCurrentIndex;
   property Options: TbUtf8Options read GetOptions write SetOptions;
   property Text: String read GetText write SetText;
 end;


  { TIbUtf8 }
{$IFDEF TB_OBJECTSTAT_ENABLED}
  TIbUtf8 = class(TbInterfacedObject, IbUtf8)
{$ELSE}
  TIbUtf8 = class(TInterfacedObject, IbUtf8)
{$ENDIF}
  private
    FText: String;
    FLength: Cardinal;
    FNumberOfChars: Cardinal;
    FCurrentByteIndex: Cardinal;
    FCurrentCharIndex: Cardinal;
    FOptions: TbUtf8Options;
  protected
    function CombinigLen(byte_index: Cardinal): Cardinal;
    procedure CharsRight(nChars: Cardinal);
    procedure CharsLeft(nChars: Cardinal);
    function GetChar(Index: Cardinal): String;
    procedure SetChar(Index: Cardinal; aValue: String);
    function GetOptions: TbUtf8Options;
    procedure SetOptions(AValue: TbUtf8Options);
    function GetCurrentIndex: Cardinal;
    procedure SetCurrentIndex(AValue: Cardinal);
    function GetText: String;
    function CopyChar(StringPos, nBytes: Cardinal): String; overload;
    function CopyChar(StringPos: Cardinal): String; overload;
    function CharLen: Cardinal;
    function CharLen(nChars: Cardinal): Cardinal;
    function CharLenBack: Cardinal;
    procedure MoveToIndex(Index: Cardinal);
    procedure UpdateByteIndexToCharIndex(ByteIndex: Integer);
    procedure SetText(AValue: String);
    procedure Init; virtual;
    function NextPos: Boolean;
    function BackPos: Boolean;
  public
    constructor Create;
    constructor Create(aText: String);
    function FirstChar: String;
    function LastChar: String;
    function NextChar: String;
    function BackChar: String;
    function IndexOf(const aValue: String): Cardinal; overload;
    function IndexOf(const aValue: String;
        StartIndex: Cardinal): Cardinal; overload;
    function LastIndexOf(const aValue: String): Cardinal; overload;
    function LastIndexOf(const aValue: String;
        StartIndex: Cardinal): Cardinal; overload;
    function IsEmpty: Boolean;
    function IsEqual(const aText: String): Boolean;
    function IsStringEnd: Boolean;
    function IsStringFront: Boolean;
    procedure LoadFromFile(const FileName: String);
    procedure LoadFromStream(Stream: TStream);
    procedure SaveToFile(const FileName: String);
    procedure SaveToStream(Stream: TStream);
    function CurrentChar: String;
    function NumberOfChars: Cardinal;
    function Length: Cardinal;
    procedure Append(aValue: IbUtf8);
    procedure Append(const aValue: String);
    function Copy: IbUtf8; overload;
    function Copy(StartIndex, nChars: Cardinal): IbUtf8; overload;
    function CopyAsString(StartIndex, nChars: Cardinal): String;
    procedure Delete(StartIndex: Cardinal); overload;
    procedure Delete(StartIndex, nChars: Cardinal); overload;
    procedure Insert(StartIndex: Cardinal; const aValue: String); overload;
    procedure Insert(StartIndex: Cardinal; aValue: IbUtf8); overload;
    property Char[Index: Cardinal]: String read GetChar write SetChar;
    property CurrentIndex: Cardinal read GetCurrentIndex write SetCurrentIndex;
    property Options: TbUtf8Options read GetOptions write SetOptions;
    property Text: String read GetText write SetText;
  end;

implementation

{ TIbUtf8 }

function TIbUtf8.GetOptions: TbUtf8Options;
begin
  Result:= FOptions;
end;

procedure TIbUtf8.SetOptions(AValue: TbUtf8Options);
begin
  FOptions:= AValue;
  Init;
end;

procedure TIbUtf8.SetCurrentIndex(AValue: Cardinal);
begin
  if FCurrentCharIndex <> AValue then begin
    if AValue > 0 then begin
      MoveToIndex(AValue);
    end else begin
      if FLength > 0 then begin
        FCurrentByteIndex:= 1;
      end else begin
        FCurrentByteIndex:= 0;
      end;
      FOptions:= FOptions + [ut8InclusiveCurrentChar];
    end;
    FCurrentCharIndex:=AValue;
  end;
end;

function TIbUtf8.CopyChar(StringPos, nBytes: Cardinal): String;
begin
  if nBytes > 0 then begin
{$PUSH}{$WARN 5094 OFF}
    SetLength(Result, nBytes);
{$POP}
    Move(FText[StringPos], Result[1], nBytes);
  end else begin
    Result:= '';
  end;
end;

function TIbUtf8.CopyChar(StringPos: Cardinal): String;
var
  nBytes: Cardinal;
begin
  nBytes:= CharLen;
  if nBytes > 0 then begin
{$PUSH}{$WARN 5094 OFF}
    SetLength(Result, nBytes);
{$POP}
    Move(FText[StringPos], Result[1], nBytes);
  end else begin
    Result:= '';
  end;
end;

function TIbUtf8.CharLen: Cardinal;
var
  c: Cardinal;
  cb: Byte;

begin
  Result:= 0;
  if (FLength > 0) and (FLength >= FCurrentByteIndex) then begin
    cb:= Byte(FText[FCurrentByteIndex]);

    if (cb and $80) = 0 then begin
      inc(Result, 1);
    end else if (cb and $E0) = $C0 then begin
      inc(Result, 2);
    end else if (cb and $F0) = $E0 then begin
      inc(Result, 3);
    end else if (cb and $F8) = $F0 then begin
      inc(Result, 4);
    end else begin
      raise EbUtf8.Create('TbUtf8.GetNumberOfChars:: Char size not found');
    end;

    if (ut8Combining in FOptions)
    and (FLength > FCurrentByteIndex)
    and (Byte(FText[FCurrentByteIndex+1]) >= $80)
    then begin
      repeat
        c:= CombinigLen(FCurrentByteIndex+Result);
        inc(Result, c);
      until c = 0;

      if (ut8HkCombiningAsOneChar in FOptions) then begin
        if ((cb and %11011111) = Byte('K')) and (Result = 3) then begin
          c:= FCurrentByteIndex + Result;
          if (FLength >= c) then begin
            cb:= Byte(FText[c]);
            if ((cb and %11011111) = Byte('H')) then begin
              inc(Result);
            end;
          end;
        end;
      end;
    end;
  end;
end;

function TIbUtf8.CharLen(nChars: Cardinal): Cardinal;
var
  cl, OldCurrentPos: Cardinal;
begin
  Result:= 0;
  OldCurrentPos:= FCurrentByteIndex;
  while (nChars > 0) do begin
    cl:= self.CharLen;
    if cl = 0 then begin
      exit;
    end;
    inc(Result, cl);
    inc(FCurrentByteIndex, cl);
    dec(nChars);
  end;
  FCurrentByteIndex:= OldCurrentPos;
end;

{$DEFINE _FAST_}

{$IFDEF _FAST_}
procedure TIbUtf8.MoveToIndex(Index: Cardinal);
var
  dr, dl: Cardinal;
begin
  if (Index < 1) or (Index > NumberOfChars ) then begin
    raise EbUtf8.Create('TbUtf8.MoveToIndex:: Index out of range');
  end;

  if Index <> FCurrentCharIndex then begin
    if Index > FCurrentCharIndex then begin
      dr:= Index - FCurrentCharIndex;
      dl:= FNumberOfChars - Index + 1;
      if dr <= dl then begin
        CharsRight(dr);
      end else begin
        if dl > 0 then begin
          FCurrentCharIndex:= FNumberOfChars;
          FCurrentByteIndex:= FLength + 1;
          CharsLeft(dl);
        end else begin
          FCurrentByteIndex:= FLength + 1;
          dec(FCurrentByteIndex, CharLenBack);
        end;
      end;
    end else begin
      dr:= Index - 1;
      dl:= FCurrentCharIndex - Index;
      if dr <= dl then begin
        FCurrentCharIndex:= 1;
        FCurrentByteIndex:= 1;
        if dr > 0 then begin
          CharsRight(dr);
        end else begin
          FCurrentByteIndex:= 1;
        end;
      end else begin
        CharsLeft(dl);
      end;
    end;
    FCurrentCharIndex:= Index;
  end;
end;
{$ELSE}

procedure TbUtf8.MoveToIndex(Index: Cardinal);
var
  cnt: Cardinal;
begin
  if (Index < 1) or (Index > NumberOfChars ) then begin
    raise EbUtf8.Create('TbUtf8.MoveToIndex:: Index out of range');
  end;

  FCurrentByteIndex:= 1;
  cnt:= Index - 1;
  while cnt > 0 do begin
    NextPos;
    dec(cnt);
  end;

  FCurrentCharIndex:= Index;
end;
{$ENDIF}

function TIbUtf8.NumberOfChars: Cardinal;
var
  OldCurrentByteIndex, OldCurrentCharIndex: Cardinal;
begin
  if (FNumberOfChars = 0) and (FLength > 0) then begin
    OldCurrentByteIndex:= FCurrentByteIndex;
    OldCurrentCharIndex:= FCurrentCharIndex;
    FCurrentCharIndex:= 0;
    FCurrentByteIndex:= 1;
    while NextPos do;
    FNumberOfChars:= FCurrentCharIndex;
    FCurrentByteIndex:= OldCurrentByteIndex;
    FCurrentCharIndex:= OldCurrentCharIndex;
  end;
  Result:= FNumberOfChars;
end;

function TIbUtf8.Length: Cardinal;
begin
  Result:= FLength;
end;

procedure TIbUtf8.Append(aValue: IbUtf8);
begin
  if aValue.Length > 0 then begin
    if FNumberOfChars = 0 then begin
      NumberOfChars;
    end;
    FText:= FText + aValue.Text;
    inc(FNumberOfChars, aValue.NumberOfChars);
    inc(FLength, aValue.Length);
  end;
end;

procedure TIbUtf8.Append(const aValue: String);
var
  lastByteIndex: Cardinal;
begin
  if aValue <> '' then begin
    if FNumberOfChars = 0 then begin
      NumberOfChars;
    end;
    lastByteIndex:= FCurrentByteIndex;
    FText:= FText + aValue;
    FCurrentByteIndex:= FLength + 1;
    inc(FLength, aValue.Length);
    while NextPos do begin
      inc(FNumberOfChars)
    end;
    FCurrentByteIndex:= lastByteIndex;
  end;
end;

function TIbUtf8.Copy(StartIndex, nChars: Cardinal): IbUtf8;
begin
  Result:= TIbUtf8.Create(CopyAsString(StartIndex, nChars));
end;

function TIbUtf8.CopyAsString(StartIndex, nChars: Cardinal): String;
var
  cl: Cardinal;
begin
  MoveToIndex(StartIndex);
  if (StartIndex + nChars - 1) > NumberOfChars then begin
    nChars:= NumberOfChars - StartIndex + 1;
  end;
  cl:= CharLen(nChars);
  Result:= System.Copy(FText, FCurrentByteIndex, cl);
end;

procedure TIbUtf8.Delete(StartIndex: Cardinal);
begin
  Delete(StartIndex, 1);
end;

procedure TIbUtf8.Delete(StartIndex, nChars: Cardinal);
var
  t: String;
  dl: Cardinal;
begin
  MoveToIndex(StartIndex);
  if ((StartIndex + nChars - 1) > NumberOfChars) then begin
    nChars:= FNumberOfChars - StartIndex + 1;
  end;
  dl:= CharLen(nChars);
  if dl >= FLength then begin
    FText:= '';
    Init;
  end else begin
{$PUSH}{$WARN 5091 OFF}
    SetLength(t, FLength - dl);
{$POP}
    Move(FText[1], t[1], FCurrentByteIndex-1);
    if FLength >= (FCurrentByteIndex + dl) then begin
      Move(FText[FCurrentByteIndex + dl], t[FCurrentByteIndex], FLength-FCurrentByteIndex-dl+1);
    end;
    if FLength > 0 then begin
      BackChar;
      dec(FLength, dl);
      dec(FNumberOfChars, nChars);
    end else begin
      Init;
    end;
    FText:= t;
  end;
end;


procedure TIbUtf8.Insert(StartIndex: Cardinal; const aValue: String);
var
  l: Cardinal;
  t: String;
begin
  l:= System.Length(aValue);
  if l > 0 then begin
    if StartIndex <> FCurrentCharIndex then begin
      if NumberOfChars >= StartIndex then begin
        MoveToIndex(StartIndex);
      end else begin
        FCurrentByteIndex:= FLength + 1;
        FCurrentCharIndex:= StartIndex;
      end;
    end;
{$PUSH}{$WARN 5091 OFF}
    SetLength(t, FLength + l);
{$POP}
    if StartIndex > 1 then begin
      Move(FText[1], t[1], FCurrentByteIndex - 1);
    end;
    Move(aValue[1], t[FCurrentByteIndex], l);
    if FLength >= FCurrentByteIndex then begin
      Move(FText[FCurrentByteIndex], t[FCurrentByteIndex+l], FLength - FCurrentByteIndex + 1);
    end;
    FText:= t;
    inc(FLength, l);
    FNumberOfChars:= 0;
  end;
end;

procedure TIbUtf8.Insert(StartIndex: Cardinal; aValue: IbUtf8);
var
  t: String;
begin
  if aValue.Length > 0 then begin
    t:= aValue.Text;
    Insert(StartIndex, t);
  end;
end;

function TIbUtf8.GetChar(Index: Cardinal): String;
begin
  MoveToIndex(Index);
  Result:= CopyChar(FCurrentByteIndex);
end;

procedure TIbUtf8.SetChar(Index: Cardinal; aValue: String);
var
  src_len, dest_len: Cardinal;
  src: IbUtf8;
  sl, sr: String;
begin
  src_len:= aValue.Length;
  MoveToIndex(Index);
  dest_len:= CharLen;
  if (src_len = 1) and (dest_len = 1)  then begin
    FText[FCurrentByteIndex]:= aValue[1];
  end else begin
    src:= TIbUtf8.Create(aValue);
    sl:= System.Copy(FText, 1, FCurrentByteIndex - 1);
    sr:= System.Copy(FText, FCurrentByteIndex + dest_len,
        FLength - FCurrentByteIndex - dest_len + 1);
    FText:= sl+aValue+sr;
    FLength:= FLength - dest_len + src_len;
    FNumberOfChars:= FNumberOfChars + src.NumberOfChars - 1;
    if FCurrentCharIndex > FNumberOfChars then begin
      FCurrentCharIndex:= FNumberOfChars;
      if FLength > 0 then begin
        FCurrentByteIndex:= FLength;
        FCurrentByteIndex:= FLength - CharLenBack;
      end else begin
        FCurrentByteIndex:= 0;
      end;
    end;
  end;
end;

function TIbUtf8.GetCurrentIndex: Cardinal;
begin
  Result:= FCurrentCharIndex;
end;

function TIbUtf8.GetText: String;
begin
  Result:= FText;
end;

function TIbUtf8.CurrentChar: String;
begin
  if (FCurrentCharIndex > 0) then begin
    Result:= GetChar(FCurrentCharIndex);
  end else begin
    Result:= '';
  end;
end;

procedure TIbUtf8.SetText(AValue: String);
begin
  FText:= AValue;
  Init;
end;

procedure TIbUtf8.Init;
begin
  FNumberOfChars:= 0;
  FLength:= FText.Length;
  if FLength > 0 then begin
    FCurrentByteIndex:= 1;
    FCurrentCharIndex:= 1;
  end else begin
    FCurrentByteIndex:= 0;
    FCurrentCharIndex:= 0;
  end;
end;

function TIbUtf8.NextPos: Boolean;
var cl: Cardinal;
begin
  cl:= CharLen;
  if cl > 0 then begin
    inc(FCurrentByteIndex, cl);
    inc(FCurrentCharIndex);
    Result:= True;
  end else begin
    Result:= False;
  end;
end;

constructor TIbUtf8.Create;
begin
  inherited;
  FOptions:= [ut8Combining, ut8HkCombiningAsOneChar];
end;

constructor TIbUtf8.Create(aText: String);
begin
  Create;
  SetText(aText);
end;

function TIbUtf8.IsEmpty: Boolean;
begin
  Result:= FText = '';
end;

function TIbUtf8.FirstChar: String;
begin
  if FLength > 0 then begin
    Result:= GetChar(1);
  end else begin
    raise EbUtf8.Create('TbUtf8.FirstChar:: String is empty');
  end;
end;

function TIbUtf8.LastChar: String;
begin
  if FLength > 0 then begin
    Result:= GetChar(NumberOfChars);
  end else begin
    raise EbUtf8.Create('TbUtf8.LastChar:: String is empty');
  end;
end;

function TIbUtf8.NextChar: String;
begin
  if ut8InclusiveCurrentChar in FOptions then begin
    Result:= CopyChar(FCurrentByteIndex);
    if not NextPos then begin
      FCurrentByteIndex:= FLength + 1;
    end;
  end else begin
    if NextPos then begin
      Result:= CopyChar(FCurrentByteIndex);
    end else begin
      Result:= '';
    end;
  end;
end;

function TIbUtf8.CombinigLen(byte_index: Cardinal): Cardinal;
var
  i: Cardinal;
  b_1, b_r: Byte;

begin
  Result:= 0;
  if byte_index < FLength then begin
    b_1:= Byte(FText[byte_index]);
    for i:= low(C_COMBINIGS) to high(C_COMBINIGS) do with C_COMBINIGS[i] do begin
      if b_1 = b1 then begin
        b_r:= Byte(FText[byte_index+1]);
        case size of
           2:  if (b_r >= b_start_value) and (b_r <= b_end_value) then begin
                 Result:= 2;
                 exit;
               end;
           3:  begin
                 if b_r = bx then begin
                   if (byte_index+2) > FLength then begin
                     exit;
                   end;
                   b_r:= Byte(FText[byte_index+2]);
                   if (b_r >= b_start_value) and (b_r <= b_end_value) then begin
                     Result:= 3;
                     exit;
                   end;
                 end;
               end;
        end; //case
      end;
    end;
  end;
end;

procedure TIbUtf8.CharsRight(nChars: Cardinal);
begin
  while nChars > 0 do begin
    NextPos;
    dec(nChars);
  end;
end;

procedure TIbUtf8.CharsLeft(nChars: Cardinal);
begin
  while nChars > 0 do begin
    BackPos;
    dec(nChars);
  end;
end;

function TIbUtf8.CharLenBack: Cardinal;
var
  p, c: Cardinal;
  b: Byte;
begin
  Result:= 0;
  p:= FCurrentByteIndex;
  while p > 1 do begin
     dec(p);
     b:= Byte(FText[p]);
     inc(Result);
     while ((b and $C0) = $80) do begin
       dec(p);
       if p = 0 then begin
         Result:= 0;
         exit;
       end;
       b:= Byte(FText[p]);
       inc(Result);
     end;
    if ut8Combining in FOptions then begin
      c:= CombinigLen(p);
      if c > 0 then begin
        Continue;
      end;

      if ut8HkCombiningAsOneChar in FOptions then begin
        if ((b and %11011111) = Byte('H')) and (p > 3) then begin
          dec(p, 2);
          c:= CombinigLen(p);
          if c > 0 then begin
            inc(Result, c);
            Continue;
          end;
        end;
      end;
    end;
    exit;
  end;
end;

function TIbUtf8.BackPos: Boolean;
var
  c: Cardinal;
begin
  c:= CharLenBack;
  if c > 0 then begin
    dec(FCurrentByteIndex, c);
    dec(FCurrentCharIndex);
    Result:= True;
  end else begin
    Result:= False;
  end;
end;

function TIbUtf8.BackChar: String;
begin
  if FLength > 0 then begin
    if ut8InclusiveCurrentChar in FOptions then begin
      Result:= CopyChar(FCurrentByteIndex);
      if not BackPos then begin
        FCurrentByteIndex:= 0;
      end;
    end else begin
      if BackPos then begin
        Result:= CopyChar(FCurrentByteIndex);
      end else begin
        Result:= '';
      end;
    end;
  end;
end;

function TIbUtf8.Copy: IbUtf8;
var
  u: TIbUtf8;
begin
  u:= TIbUtf8.Create(FText);
  u.FNumberOfChars:= FNumberOfChars;
  Result:= u;
end;

procedure TIbUtf8.UpdateByteIndexToCharIndex(ByteIndex: Integer);
var
  ci: Cardinal;
begin
  inc(ByteIndex);
  FCurrentByteIndex:= 1;
  ci:= 1;
  while (ByteIndex <> FCurrentByteIndex) do begin
    if not NextPos then begin
      raise EbUtf8.Create('UpdateByteIndexToCharIndex:: Chr index not found');
    end;
    inc(ci);
  end;
  FCurrentCharIndex:= ci;
end;

function TIbUtf8.IndexOf(const aValue: String): Cardinal;
begin
  Result:= IndexOf(aValue, 1);
end;

function TIbUtf8.IndexOf(const aValue: String; StartIndex: Cardinal): Cardinal;
var
  r: Integer;
begin
  if FLength > 0 then begin
    MoveToIndex(StartIndex);
    r:= FText.IndexOf(aValue, FCurrentByteIndex-1);
    if r >= 0 then begin
      UpdateByteIndexToCharIndex(r);
      Result:= FCurrentCharIndex;
    end else begin
      Result:= 0;
    end;
  end else begin
    Result:= 0;
  end;
end;

function TIbUtf8.LastIndexOf(const aValue: String): Cardinal;
begin
  Result:= LastIndexOf(aValue, 1);
end;

function TIbUtf8.LastIndexOf(const aValue: String;
    StartIndex: Cardinal): Cardinal;
var
  r: Integer;
begin
  if FLength > 0 then begin
    MoveToIndex(StartIndex);
    r:= FText.LastIndexOf(aValue, FLength);
    if r >= 0 then begin
      UpdateByteIndexToCharIndex(r);
      Result:= FCurrentCharIndex;
    end else begin
      Result:= 0;
    end;
  end else begin
    Result:= 0;
  end;
end;

//On Empty String True
function TIbUtf8.IsStringEnd: Boolean;
begin
  Result:= (NumberOfChars = 0) or (FCurrentCharIndex >= FNumberOfChars);
end;

//On Empty String True
function TIbUtf8.IsStringFront: Boolean;
begin
  Result:= (FLength = 0) or (FCurrentByteIndex = 1);
end;

function TIbUtf8.IsEqual(const aText: String): Boolean;
begin
  Result:= aText = FText;
end;

procedure TIbUtf8.SaveToFile(const FileName: String);
var
  ms: TMemoryStream;
begin
  try
    ms:= TMemoryStream.Create;
    if FLength > 0 then begin
      ms.Write(FText[1], FLength);
    end;
    ms.SaveToFile(FileName);
  finally
    FreeAndNil(ms);
  end;
end;

procedure TIbUtf8.SaveToStream(Stream: TStream);
begin
  if FLength > 0 then begin
    Stream.Write(FText[1], FLength);
  end;
end;

procedure TIbUtf8.LoadFromFile(const FileName: String);
var
  ms: TMemoryStream;
begin
  try
    ms:= TMemoryStream.Create;
    ms.LoadFromFile(FileName);
    ms.Position:= 0;
    if ms.Size > 0 then begin
      SetLength(FText, ms.Size);
      ms.Read(FText[1], ms.Size);
    end else begin
      FText:= '';
    end;
    Init;
  finally
    FreeAndNil(ms);
  end;
end;

procedure TIbUtf8.LoadFromStream(Stream: TStream);
var
  d: Cardinal;
begin
  d:= Stream.Size - Stream.Position;
  if d > 0 then begin
    SetLength(FText, d);
    Stream.Read(FText[1], d);
  end else begin
    FText:= '';
  end;
  Init;
end;

end.
