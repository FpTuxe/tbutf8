unit tb_utf8_combinings;

{
  File: tb_utf8_combinings.pas
  CreateDate: 23.12.2021
  ModifyDate: 18.11.2022
  Version: 1.0.0.3
  Author: Thomas Bulla

  See the file COPYING.TXT, included in this distribution,
  for details about the copyright.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
}

{$mode objfpc}{$H+}

interface

type
  TCombiningItem = record
    size: Byte;
    b1: Byte;
    bx: Byte;
    b_start_value: Byte;
    b_end_value: Byte;
  end;

  TCombinings = array of TCombiningItem;

//The unicode combinings comes from here
//https://www.compart.com/en/unicode/

const
  C_COMBINIGS: TCombinings = (                                         //Unicode
    (size: $02; b1:$CC; bx: $00; b_start_value:$80; b_end_value: $BF), //U+0300 - U+036F
    (size: $02; b1:$CD; bx: $00; b_start_value:$80; b_end_value: $AF), //U+0340 - U+036F
    (size: $03; b1:$E1; bx: $B7; b_start_value:$80; b_end_value: $BF), //U+1DC0 - U+1DFF
    (size: $03; b1:$E1; bx: $AA; b_start_value:$B0; b_end_value: $BF), //U+1AB0 - U+1ABF
    (size: $03; b1:$E1; bx: $AB; b_start_value:$80; b_end_value: $80)  //U+1AC0 - U+1AC0
  );

implementation

end.

