{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit tbutf8;

{$warn 5023 off : no warning about unused units}
interface

uses
  tb_utf8, tb_utf8_combinings, LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('tbutf8', @Register);
end.
